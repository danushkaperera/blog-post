require('./bootstrap');
import Vue from  'vue';
window.Vue = require('vue');


import VueAxios from 'vue-axios';
import axios from 'axios';
import Posts from './components/posts';
 
Vue.use(VueAxios, axios);
 
const app = new Vue({
    el: '#app',
    components:{Posts}
});
