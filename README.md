# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* This is for simple blog post displaying project with using laravel and vuejs
* Laravel Version 8
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### This is how get set up? ###

clone this project with using this command in you command line:
```
 git clone https://danushkaperera@bitbucket.org/danushkaperera/blog-post.git

```
jump in to the folder with below command 
```
cd blog-post

```
update the composer using below command
``` 
composer update

```
install npm using below command
``` 
npm install

```
we dont use any databases here so easy to run the project with

```
 npm run dev & php artisan serve

```


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact